import discord
import random
from dotenv import load_dotenv
import os
load_dotenv()
DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")

MY_TAG = '<@!351857809362780162>'
BOT_TAG = '<@!505812453230379028>'

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    await _try_self_promoter(message)

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')

    print(message.author)
    print(message.content)

async def _try_self_promoter(message):
    lower_content = message.content.lower()
    if (BOT_TAG in lower_content):
        responses = ['What do you want?', 'Don''t waste my time.', 'Begone!']
        await message.channel.send(responses[random.randint(0, len(responses) - 1)])
    return
    

client.run(DISCORD_BOT_TOKEN)